//
//  InterfaceController.swift
//  watchGame Extension
//
//  Created by satram prudhvi on 2019-10-31.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity



class InterfaceController: WKInterfaceController , WCSessionDelegate  {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    var gameInt = 30
    var gameTimer = Timer()
    @IBOutlet weak var timelabel: WKInterfaceLabel!
    @IBOutlet var skInterface: WKInterfaceSKScene!
    
    @IBOutlet weak var messagelabel: WKInterfaceLabel!
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)        
    }
    
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]){
         print("watch is receving messages")
         let name = message["time"] as! String
        print(name)
             messagelabel.setText("WARNING::\(name)")
     
         WCSession.default.sendMessage(message, replyHandler: nil)
     }
    
    
    @IBAction func swipeRight(_ sender: Any) {
        
        print("swipe Right")
        
                   if(WCSession.default.isReachable == true){
                   let message = ["name":"right"] as [String : Any]
                   WCSession.default.sendMessage(message, replyHandler: nil)
                   }
                   else {
                   print("NOT REACHABLE")
                   }
        
        
    }
    
    @IBAction func swipeLeft(_ sender: Any) {
                         print("swipe Left")
                         if(WCSession.default.isReachable == true){
                          let message = ["name":"left"] as [String : Any]
                          WCSession.default.sendMessage(message, replyHandler: nil)
                          }
                          else {
                          print("not reachable")
                          }

        
    }
    
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        
        print("WATCH IS ON")

        super.willActivate()
        
        // @TODO: Does the phone support communication with the watch?
        if (WCSession.isSupported() == true) {
    // create a communication session with the watch
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
print("not reachable")

        }
    }
    
    @IBAction func pauseGame() {
        
             let message = ["name":"pause"] as [String : Any]
                               WCSession.default.sendMessage(message, replyHandler: nil)
        
    }
    
    @IBAction func powerup() {
        
        let message = ["name":"powerup"] as [String : Any]
                          WCSession.default.sendMessage(message, replyHandler: nil)
    }
    
    
    @IBAction func resumegame() {
        
        let message = ["name":"resume"] as [String : Any]
                          WCSession.default.sendMessage(message, replyHandler: nil)

        
    }
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    

}
