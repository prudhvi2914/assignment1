//
//  GameScene.swift
//  SushiTower
//
//  Created by Prudhvi on 2019-02-14.
//


import SpriteKit
import GameplayKit
import WatchConnectivity
//import Firebase



class GameScene: SKScene,WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    
    let cat = SKSpriteNode(imageNamed: "character1")
    //Added fir
    let sushiBase = SKSpriteNode(imageNamed:"roll")
    var highscoreLabel:SKLabelNode!


    // Make a tower
    var sushiTower:[SushiPiece] = []
    let SUSHI_PIECE_GAP:CGFloat = 80
    var catPosition = "left"
    
    // Show life and score labels
    let lifeLabel = SKLabelNode(text:"Lives: ")
    var scoreLabel = SKLabelNode(text:"Score: ")
    //var ref:DatabaseReference?
    
    var lives = 5
    var score = 0
    var highscore = 0

  var  gameIsRunning = false
//adding a countdowntimer
   var gameInt = 25
    let countDownLabel = SKLabelNode(text:"TimeRemaining: ")
    var numloops = 0
    
    func spawnSushi() {
        // 1. Make a sushi
        let sushi = SushiPiece(imageNamed:"roll")
        
        // 2. Position sushi 10px above the previous one
        if (self.sushiTower.count == 0) {
            // Sushi tower is empty, so position the piece above the base piece
            sushi.position.y = sushiBase.position.y
                + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        else {
            // OPTION 1 syntax: let previousSushi = sushiTower.last
            // OPTION 2 syntax:
            let previousSushi = sushiTower[self.sushiTower.count - 1]
            sushi.position.y = previousSushi.position.y + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        
        // 3. Add sushi to screen
        addChild(sushi)
        
        // 4. Add sushi to array
        self.sushiTower.append(sushi)
    }
  
    override func didMove(to view: SKView) {
        //communication session between watch and phone
        if(WCSession.isSupported() == true){
            print("connected")
         let session = WCSession.default
         session.delegate = self
         session.activate()
         }
         else {
            print("Not connecting")
         }
        // add background
        let background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        // add cat
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        addChild(cat)
        
        // add base sushi pieces
        sushiBase.position = CGPoint(x:self.size.width*0.5, y: 100)
        addChild(sushiBase)
        
        // build the tower
        self.buildTower()
        
        // Game labels
        self.scoreLabel = SKLabelNode(text: "SCORE: \(self.score)")

        self.scoreLabel.position.x = 100
        self.scoreLabel.zPosition = 3
        self.scoreLabel.fontColor = UIColor.red

        self.scoreLabel.position.y = size.height - 100
        self.scoreLabel.fontName = "Avenir"
        self.scoreLabel.fontSize = 40
        addChild(scoreLabel)
        // Add a score label
        self.highscoreLabel = SKLabelNode(text: "HighScore: \(self.highscore)")
        self.highscoreLabel.position = CGPoint(x:100, y:500)
               self.highscoreLabel.fontColor = UIColor.red
                          self.highscoreLabel.zPosition = 3
                          self.highscoreLabel.fontSize = 30
                          self.highscoreLabel.fontName = "Avenir"
                          addChild(self.highscoreLabel)
        
        
        // Game labels
        self.countDownLabel.position.x = 100
        self.countDownLabel.position.y = size.height - 200
        self.countDownLabel.fontName = "Avenir"
        self.countDownLabel.zPosition = 3
        self.countDownLabel.fontSize = 40
        addChild(countDownLabel)
        
        // Life label
        self.lifeLabel.position.x = 100
        self.lifeLabel.position.y = size.height - 150
        self.lifeLabel.fontName = "Avenir"
        self.lifeLabel.fontSize = 40
        addChild(lifeLabel)
        
        
        //let ref = Database.database().reference()
    }
    @IBAction func addUserScore(_ sender: Any){
        
        
    }
    
    func buildTower() {
        for _ in 0...10 {
            self.spawnSushi()
        }
    }
    
    

    
    
    override func update(_ currentTime: TimeInterval) {
        numloops = numloops + 1;
        if (gameIsRunning == false){
              if(numloops%100 == 0){

                 // countDownTimer()
                gameInt -= 1
                countDownLabel.text = "TimeRemaining:\(String(gameInt))"
            
        
            if (gameInt == 20 || gameInt == 15 || gameInt == 10)
                                {
                                
                   print("You are running out of time")
                   let message = ["time": "\(gameInt)"]
                   WCSession.default.sendMessage(message, replyHandler: nil)
                         
        
                               }
            if (gameInt == 0){
                countDownLabel.text = "GameOver:\(String(gameInt))"
                gameIsRunning = true
                
                       
            }

            }}
        var defaults = UserDefaults()
                               var highscore=defaults.integer(forKey: "highscore")

                               if(score>highscore)
                               {
//                                try {
//                                    self.ref?.child("prudhvi").childByAutoId().setValue("\(score)")}

                                   defaults.set(score, forKey: "highscore")
                      
                               }
                               var highscoreshow=defaults.integer(forKey: "highscore")

                               highscoreLabel.text="HighSCORE: \(highscoreshow)"
        
    }
    

    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]){
        let name = message["name"] as? String
        if(name == "right"){catmovingRight()}
        if(name == "left"){ catmovingLeft()}
        
        if(name == "powerup"){
            gameInt += 10
        }
        if(name == "pause" ){
            gameIsRunning = true
        }
        if(name == "resume" ){
                   gameIsRunning = false
               }
        
        }
    

    
    func catmovingLeft(){
  
                       // 2. person clicked left, so move cat left
                       cat.position = CGPoint(x:self.size.width*0.25, y:100)
        
                       // change the cat's direction
                       let facingRight = SKAction.scaleX(to: 1, duration: 0)
                       self.cat.run(facingRight)
        
                       // save cat's position
                       self.catPosition = "left"
                    // -------------------------------------
                         let pieceToRemove = self.sushiTower.first
                         if (pieceToRemove != nil) {
                             // SUSHI: hide it from the screen & remove from game logic
                             pieceToRemove!.removeFromParent()
                             self.sushiTower.remove(at: 0)
                             
                             // SUSHI: loop through the remaining pieces and redraw the Tower
                             for piece in sushiTower {
                                 piece.position.y = piece.position.y - SUSHI_PIECE_GAP
                             }
                             
                             // To make the tower inifnite, then ADD a new piece
                             self.spawnSushi()
                         }

        
                   
        
        
    }
    
    func catmovingRight(){

                       print("TAP RIGHT")
                       // 2. person clicked right, so move cat right
                       cat.position = CGPoint(x:self.size.width*0.85, y:100)
        
                       // change the cat's direction
                       let facingLeft = SKAction.scaleX(to: -1, duration: 0)
                       self.cat.run(facingLeft)
        
                       // save cat's position
                       self.catPosition = "right"
                    // -------------------------------------
                         let pieceToRemove = self.sushiTower.first
                         if (pieceToRemove != nil) {
                             // SUSHI: hide it from the screen & remove from game logic
                             pieceToRemove!.removeFromParent()
                             self.sushiTower.remove(at: 0)
                             
                             // SUSHI: loop through the remaining pieces and redraw the Tower
                             for piece in sushiTower {
                                 piece.position.y = piece.position.y - SUSHI_PIECE_GAP
                             }
                             
                             // To make the tower inifnite, then ADD a new piece
                             self.spawnSushi()
                         }
    
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
  
        guard let mousePosition = touches.first?.location(in: self) else {
            return
        }

        print(mousePosition)
        
        // ------------------------------------
        // MARK: UPDATE THE SUSHI TOWER GRAPHICS
        //  When person taps mouse,
        //  remove a piece from the tower & redraw the tower
        // -------------------------------------
        let pieceToRemove = self.sushiTower.first
        if (pieceToRemove != nil) {
            // SUSHI: hide it from the screen & remove from game logic
            pieceToRemove!.removeFromParent()
            self.sushiTower.remove(at: 0)
            
            // SUSHI: loop through the remaining pieces and redraw the Tower
            for piece in sushiTower {
                piece.position.y = piece.position.y - SUSHI_PIECE_GAP
            }
            
            // To make the tower inifnite, then ADD a new piece
            self.spawnSushi()
        }
    
        // 1. detect where person clicked
        let middleOfScreen  = self.size.width / 2
        if (mousePosition.x < middleOfScreen) {
            print("TAP LEFT")
            // 2. person clicked left, so move cat left
            cat.position = CGPoint(x:self.size.width*0.25, y:100)
            
            // change the cat's direction
            let facingRight = SKAction.scaleX(to: 1, duration: 0)
            self.cat.run(facingRight)
            
            // save cat's position
            self.catPosition = "left"
            
        }
        else {
            print("TAP RIGHT")
            // 2. person clicked right, so move cat right
            cat.position = CGPoint(x:self.size.width*0.85, y:100)
            
            // change the cat's direction
            let facingLeft = SKAction.scaleX(to: -1, duration: 0)
            self.cat.run(facingLeft)
            
            // save cat's position
            self.catPosition = "right"
        }

        // ------------------------------------
        // MARK: ANIMATION OF PUNCHING CAT
        // -------------------------------------
        
        // show animation of cat punching tower
        let image1 = SKTexture(imageNamed: "character1")
        let image2 = SKTexture(imageNamed: "character2")
        let image3 = SKTexture(imageNamed: "character3")
        
        let punchTextures = [image1, image2, image3, image1]
        
        let punchAnimation = SKAction.animate(
            with: punchTextures,
            timePerFrame: 0.1)
        
        self.cat.run(punchAnimation)

        
        
        
        if (self.sushiTower.count > 0) {
            // 1. if CAT and STICK are on same side - OKAY, keep going
            // 2. if CAT and STICK are on opposite sides -- YOU LOSE
            let firstSushi:SushiPiece = self.sushiTower[0]
            let chopstickPosition = firstSushi.stickPosition
            
            if (catPosition == chopstickPosition) {
                // cat = left && chopstick == left
                // cat == right && chopstick == right
                print("Cat Position = \(catPosition)")
                print("Stick Position = \(chopstickPosition)")
                print("Conclusion = LOSE")
                print("------")
                
                self.lives = self.lives - 1
                self.lifeLabel.text = "Lives: \(self.lives)"
            }
            else if (catPosition != chopstickPosition) {
                // cat == left && chopstick = right
                // cat == right && chopstick = left
                print("Cat Position = \(catPosition)")
                print("Stick Position = \(chopstickPosition)")
                print("Conclusion = WIN")
                print("------")
                
                self.score = self.score + 10
                self.scoreLabel.text = "Score: \(self.score)"
            }
        }
        
        else {
            print("Sushi tower is empty!")
        }
        
    }

    
    
    @objc func countDownTimer(){
//
//          let timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countDownTimer), userInfo: nil, repeats: true)

//          if(gameInt > 0){
//          countDownLabel.text = "TimeRemaining:\(String(gameInt))"
//            gameIsRunning = true
//          }
//           else if(gameInt == 0){
//           countDownLabel.text = "GameOver"
////            scoreLabel.text = "Score: \(self.score)"
//           gameIsRunning = false
//            countDownLabel.text = "Game Over"
//
//           }
        }
 
}
